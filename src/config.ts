import scenes from './scenes/index';
export default {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 }
        }
    },
    scene: scenes,
    parent: 'phaser-content'
};
