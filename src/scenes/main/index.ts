class BootScene extends Phaser.Scene {
    constructor () {
      super({ key: 'BootScene' })
      console.log('BootScene created!')
    }
  
    preload () {
    }
  
    create () {
      this.scene.start('dishwasher')
    }
  }
  
  export default BootScene