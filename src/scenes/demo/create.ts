import { BlendModes } from "phaser";

export default function(game: Phaser.Scene)
{
    game.add.image(400, 300, 'sky');

    var particles = game.add.particles('red');
    
    const particalEmitterConfig = {
        speed: 100,
        scale: { start: 1, end: 0 },
        blendMode: BlendModes.ADD
    } as ParticleEmitterConfig;
    var emitter = particles.createEmitter(particalEmitterConfig);

    var logo = game.physics.add.image(400, 100, 'logo');

    logo.setVelocity(100, 200);
    logo.setBounce(1, 1);
    logo.setCollideWorldBounds(true);

    emitter.startFollow(logo);
}