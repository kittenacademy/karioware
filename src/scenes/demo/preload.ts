import { Scene } from "phaser";

export default function(scene: Scene)
{
    console.log('preloading demo');
    scene.load.setBaseURL('http://labs.phaser.io');

    scene.load.image('sky', 'assets/skies/space3.png');
    scene.load.image('logo', 'assets/sprites/phaser3-logo.png');
    scene.load.image('red', 'assets/particles/red.png');
}