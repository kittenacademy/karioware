import _preload from './preload';
import _create from './create';
import _update from './update';

class GameScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'demo',
            physics: {
                system: 'impact',
                gravity: 100,
                setBounds: {
                    width: 800,
                    height: 600,
                }
            }
        });
    }

    preload = () => _preload(this)
    create = () => _create(this)
    update () {
        _update(this);
    }
}

export default GameScene;